package com.finalexam.trujillo.app.accounts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExamServiceAccountsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExamServiceAccountsApplication.class, args);
    }

}
