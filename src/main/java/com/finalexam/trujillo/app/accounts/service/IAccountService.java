package com.finalexam.trujillo.app.accounts.service;

import com.finalexam.trujillo.app.accounts.model.entity.Account;
import com.finalexam.trujillo.app.accounts.model.response.AccountResponse;
import io.reactivex.Single;

public interface IAccountService {


    Single<AccountResponse> obtainAccount(String card);


}
