package com.finalexam.trujillo.app.accounts.model.entity;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Account {


    private String accountNumber;
    private Double amount;

}
