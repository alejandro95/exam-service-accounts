package com.finalexam.trujillo.app.accounts.model.response;


import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AccountResponse {


    private String accountNumber;
    private Double amount;


}
