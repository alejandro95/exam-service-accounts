package com.finalexam.trujillo.app.accounts.controller;


import com.finalexam.trujillo.app.accounts.model.response.AccountResponse;
import com.finalexam.trujillo.app.accounts.service.IAccountService;
import io.reactivex.Single;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/core")
public class AccountController {

    @Autowired
    private  IAccountService iAccountService;

    @GetMapping("/accounts")
    public Single<AccountResponse> obtainAccount(@RequestParam String cardNumber){

        return iAccountService.obtainAccount(cardNumber);
    }


}
