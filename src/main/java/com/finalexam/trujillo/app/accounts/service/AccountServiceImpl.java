package com.finalexam.trujillo.app.accounts.service;

import com.finalexam.trujillo.app.accounts.model.entity.Account;
import com.finalexam.trujillo.app.accounts.model.response.AccountResponse;
import io.reactivex.Single;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class AccountServiceImpl implements IAccountService {

    @Override
    public Single<AccountResponse> obtainAccount(String cardNumber) {

        List<AccountResponse> accounts = Arrays.asList(
                AccountResponse.builder()
                        .accountNumber("1111222233334441")
                        .amount(1000.00)
                        .build(),
                AccountResponse.builder()
                        .accountNumber("1111222233334442")
                        .amount(500.00)
                        .build(),
                AccountResponse.builder()
                        .accountNumber("1111222233334443")
                        .amount(1500.00)
                        .build());

        AccountResponse accountResponse = accounts.stream()
                .filter(account-> account.getAccountNumber().equals(cardNumber))
                .peek(account -> account.setAccountNumber(account.getAccountNumber()+"XXX"))
                .findFirst()
                .orElseThrow(NullPointerException::new);

        return Single.just(accountResponse);
    }
}
