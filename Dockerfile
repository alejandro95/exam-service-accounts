FROM maven:3.6-jdk-8-alpine AS builder
VOLUME /tmp
ADD ./target/exam-service-accounts-0.0.1-SNAPSHOT.jar service-accounts.jar
ENTRYPOINT ["java","-jar","/service-accounts.jar"]